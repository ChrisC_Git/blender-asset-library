# Blender Asset Library

A place to store my Blender Asset Library, probably going to mostly be geo nodes... but lets see where this goes!
I have decided to store the nodes in a blender version folder. This is just to future proof any nodes created that are specifically only usable in that version.

## Geo Nodes
- **Mountain** - Emulating the node that Houdini has to push geo based on a noise texure. The Distortion attrib allows for animation over time.
- **Scatter** - A simple scatter tool that takes a Geo Surface and an Instance Object to scatter on to the surface. Allows you to control Density, Scale & Rotation of the instanced object. Currently works for grass or objects on a Z facing normal and face normals via a switch. May make a more advanced scatter for complex surfaces.
- **Scatter Volume** - Scatters points inside of a volume at a specified resolution.
- **Camera Culling** - A simple fields based (via Raycast) procedural camera culling system. I have not added any lens mm to FOV conversion just yet, so currenlty you need to specify the 'size' of the lens. This does allow you to control overscan which can be useful.
- **Simple Terrian Generator** - A simple noise based terrian generator.
- **Large Scale Terrian Generator** - A large scale, high poly terrian generator
- **Delete Intersecting Faces** - Quick and easy way to detect and delete intersecting geo faces. Add this network to one object & drag in the 'Target Geo' via an Object Info (relative) node.